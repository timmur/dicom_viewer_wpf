﻿using System;
using System.Windows;
using System.Windows.Media;

using DICOMViewer.Helper;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using DICOMViewer.Parsing;
using DICOMViewer.Volume;

namespace DICOMViewer
{
    /// <summary>
    /// Логика взаимодействия для SegmentView.xaml
    /// </summary>
    /// 


    public struct Position
    {
        public Point3D Index;
        public Point Image;
        public Point3D Absolute;
    }
    public partial class SegmentView : Window
    {
        private List<IOD> mIODList;
        private CTSliceInfo slice;
        private List<CTSliceInfo> mSlices;
        private int idx;
        
        private Position A;
        private Position B;

        bool dragging;
        WriteableBitmap bi; 


        public SegmentView(List<IOD> aIODList)
        {
            InitializeComponent();
            
            mIODList = aIODList;
            mSlices = new List<CTSliceInfo>();
            foreach (IOD anIOD in mIODList)
            {
                CTSliceInfo aCTSliceInfo = new CTSliceInfo(anIOD.XDocument, anIOD.FileName);
                aCTSliceInfo.Image_Bitmap = aCTSliceInfo.GetPixelBufferAsBitmap();  // загружаться будет долго, зато смена снимка в окне будет срабатывать молниеносно
                mSlices.Add(aCTSliceInfo);
            }
            slice = mSlices[0];
            dragging = false;
            idx = DICOMParserUtility.GetDICOMAttributeAsInt(slice.XDoc, "(0020,0013)") - 1;

            slice.Image_Bitmap = slice.GetPixelBufferAsBitmap();
            mImage.Source = slice.Image_Bitmap;
            bi = (WriteableBitmap)mImage.Source; mSelection.Margin = new Thickness(0, 0, 0, 0); mSelection.Height = mSelection.Width = 0;
            note.Text = string.Format("Legit 'z' values: 1 - {0}. Current slice number is {1}. \nUse mouse wheel to show next/previous slice.", mIODList.Count.ToString(), (idx + 1).ToString());

        }

        // change HU
        public void GetNewImage(object sender, RoutedEventArgs e)
        {
            //проверяем
            int tb1, tb2;
            if (Int32.TryParse(TB1.Text, out tb1) == false || Int32.TryParse(TB2.Text, out tb2) == false) { MessageBox.Show("Invalid input!"); return; }
            //обнуляем
            mImage.Source = null;
            slice.Image_Bitmap = null;
            //задаем новые
            mImage.Source = slice.GetPixelBufferAsBitmap(tb1, tb2);
            slice.Image_Bitmap = null;
            slice.Image_Bitmap = slice.GetPixelBufferAsBitmap();
        }

        //mouse down
        public void SelectPixel(object sender, MouseEventArgs e)
        {
            if (dragging) return;

            A = new Position(); B = new Position();
            //найдем тут индексы пикселя в битмапе
            A.Index.X = (int)((e.GetPosition(mImage)).X * bi.PixelWidth / mImage.ActualWidth);
            A.Index.Y = (int)((e.GetPosition(mImage)).Y * bi.PixelHeight / mImage.ActualHeight);

            //а тут зафиксируем положение в общей СК
            A.Absolute.X = slice.UpperLeft_X + A.Index.X * slice.PixelSpacing_X;
            A.Absolute.Y = slice.UpperLeft_Y + A.Index.Y * slice.PixelSpacing_Y;
            A.Absolute.Z = slice.UpperLeft_Z;

            //теперь можно рисовать
            A.Image.X = e.GetPosition(mImage).X;
            A.Image.Y = e.GetPosition(mImage).Y;
            mSelection.Margin = new Thickness(A.Image.X, A.Image.Y, 0, 0);
            mSelection.Width = mSelection.Height = 1;
            dragging = true;
        }

        public void Drag(object sender, MouseEventArgs e)
        {
            if (!dragging) return;
            B.Image.X = e.GetPosition(mImage).X;
            B.Image.Y = e.GetPosition(mImage).Y;
            if (A.Image.X > B.Image.X || A.Image.Y > B.Image.Y) return;
            mSelection.Width = B.Image.X - A.Image.X;
            mSelection.Height = B.Image.Y - A.Image.Y;
        }
        //mouse up
        public void EndDrag(object sender, MouseEventArgs e)
        {
            if (!dragging) return;
            else dragging = false;
            //найдем тут индексы пикселя в битмапе
            B.Index.X = (int)((e.GetPosition(mImage)).X * bi.PixelWidth / mImage.ActualWidth);
            B.Index.Y = (int)((e.GetPosition(mImage)).Y * bi.PixelHeight / mImage.ActualHeight);

            //а тут зафиксируем положение в общей СК
            B.Absolute.X = slice.UpperLeft_X + B.Index.X * slice.PixelSpacing_X;
            B.Absolute.Y = slice.UpperLeft_Y + B.Index.Y * slice.PixelSpacing_Y;
            B.Absolute.Z = slice.UpperLeft_Z;

            return;
        }

        public void CreateSegment(object sender, RoutedEventArgs e)
        {
            int _z1, _z2;
            if (Int32.TryParse(z1.Text, out _z1) == false || Int32.TryParse(z2.Text, out _z2) == false) { MessageBox.Show("Invalid input!"); return; }
            List<IOD> aIODList = new List<IOD>();
            for (int i = _z1 - 1; i < _z2; i++)
                aIODList.Add(mIODList[i]);

            VolumeView aVolumeViewWindow = new VolumeView(1);
            aVolumeViewWindow.CreatePartialVolume(aIODList, 500, A, B);
            aVolumeViewWindow.Title = string.Format("DICOM Viewer - Volume View (IsoValue = +500 in Hounsfield Units)");
            aVolumeViewWindow.Show();
        }

        public void ExcludeSegment(object sender, RoutedEventArgs e)
        {
            int _z1, _z2;
            if (Int32.TryParse(z1.Text, out _z1) == false || Int32.TryParse(z2.Text, out _z2) == false) { MessageBox.Show("Invalid input!"); return; }
            A.Index.Z = (double)_z1;
            B.Index.Z = (double)_z2;
            VolumeView aVolumeViewWindow = new VolumeView(2);
            aVolumeViewWindow.CreateCutVolume(mIODList, 500, A, B);
            aVolumeViewWindow.Title = string.Format("DICOM Viewer - Volume View (IsoValue = +500 in Hounsfield Units)");
            aVolumeViewWindow.Show();
        }


        public void UserControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            if (e.Delta > 0 && idx < mSlices.Count - 1)
                    {
                        idx++;
                        slice = mSlices[idx]; mImage.Source = slice.Image_Bitmap;
                        note.Text = string.Format("Legit 'z' values: 1 - {0}. Current slice number is {1}. \nUse mouse wheel to show next/previous slice.", mIODList.Count.ToString(), (idx + 1).ToString());
                    }
            if (e.Delta < 0 && idx > 0)
                    {
                        idx--;
                        slice = mSlices[idx]; mImage.Source = slice.Image_Bitmap;
                        note.Text = string.Format("Legit 'z' values: 1 - {0}. Current slice number is {1}. \nUse mouse wheel to show next/previous slice.", mIODList.Count.ToString(), (idx + 1).ToString());
                    }
            
        }
    }
}
